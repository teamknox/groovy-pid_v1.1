/**
 * @file      UART.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "UART.h"
#include "io_define.h"

// Read & Write pointers into the serial circular buffer
volatile static char *gPtrTxWrite;
volatile static char *gPtrTxRead;

// Number of bytes currently in the serial circular buffer
volatile static char gTxNumBytes = 0;

void InitUART(void)
{
    gPtrTxWrite = &gTxBuffer[0];
    gPtrTxRead = &gTxBuffer[0];
    gTxNumBytes = 0;

    TXxIF = 0;
    TXxIP = 0;
    TXxIE = 0;
  
    RCSTAx   = 0b10010000;
    TXSTAx   = 0b00100100;
    BAUDCONx = 0b00001000;
//    SPBRGx   = 207;             // 9600bps
    SPBRGx   = 68;             // 57600bps

}


void putch (unsigned char aC)
{
    TXxIE = 0;
    if (gTxNumBytes == 0 && TX2IF){
        TXREGx = aC;
    }
    else{
        if(gTxNumBytes == TX_BUFFER_SIZE){
            TXxIE = 1;
            while(gTxNumBytes == TX_BUFFER_SIZE);
            TXxIE = 0;
        }
        *gPtrTxWrite = aC;
        gPtrTxWrite++;
        if(gPtrTxWrite == gTxBuffer + TX_BUFFER_SIZE){
            gPtrTxWrite = gTxBuffer;
        }
        gTxNumBytes++;
        TXxIE = 1;
    }
}

/*
void putch(unsigned char ch) {
    while (!TXxIF);
        TXREGx = ch;
        LED = LED_OFF;
}
*/

void UartTxISR(void)
{
    TXREGx = *gPtrTxRead;
    gPtrTxRead++;
    if(gPtrTxRead == gTxBuffer + TX_BUFFER_SIZE){
        gPtrTxRead = gTxBuffer;
    }
    gTxNumBytes--;
    if(gTxNumBytes == 0){
        TXxIE = 0;
    }
}

unsigned char myGetch()
{
    while(!RCxIF)   // Set when register is not empty
        continue;
    return RCREGx;
}

unsigned char myGetche()
{
    unsigned char c;
    putch(c=myGetch());
    return c;
}

unsigned char myKbhit(void)
{
    return RCxIF;
}

void myPuts(const char *s)
{
    while(*s){
        putch(*s);
        s++;
    }
}
