/**
 * @file      channel.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "channel.h"

extern int gDesiredTarget[MAX_CHANNEL];
extern volatile long gTimer1ms;

void SetPWM(unsigned char aCh, unsigned int aDutyCycle)
{
    fptrPWM pwm;
    pwm = PWM[aCh];
    pwm(aDutyCycle);
}
void PWM0(unsigned int aDutyCycle)
{
    REG_0_PWM_H = aDutyCycle >> 2;
    REG_0_PWM_M = (aDutyCycle >> 1) & 0x01;
    REG_0_PWM_L = aDutyCycle & 0x01;
}
void PWM1(unsigned int aDutyCycle)
{
    REG_1_PWM_H = aDutyCycle >> 2;
    REG_1_PWM_M = (aDutyCycle >> 1) & 0x01;
    REG_1_PWM_L = aDutyCycle & 0x01;
}

void SetBrake(unsigned char aCh)
{
    fptrBrake brake;
    brake = Brake[aCh];
    brake();
}
void Brake0()
{
    INP1_L = 1;
    INP2_L = 1;
}
void Brake1()
{
    INP3_L = 1;
    INP4_L = 1;    
}


void SetForward(unsigned char aCh)
{
    fptrForward forward;
    forward = Forward[aCh];
    forward();
}
void Forward0()
{
    INP1_L = 1;
    INP2_L = 0;
}
void Forward1()
{
    INP3_L = 1;
    INP4_L = 0;    
}

void SetBackward(unsigned char aCh)
{
    fptrBackward backward;
    backward = Backward[aCh];
    backward();    
}
void Backward0()
{
    INP1_L = 0;
    INP2_L = 1;    
}
void Backward1()
{
    INP3_L = 0;
    INP4_L = 1;    
}

void GateTimeHandling()
{
    unsigned char flgChannel = 0b00000001;
    int i;
    
    for (i = 0;i < USED_CHANNEL;i++){
        if(bittst(gFlgGate, flgChannel)){
            if(gGateTime[i] == 0){
                bitclr(gFlgGate, flgChannel);
                if(gFlgTzEnable){
                    gDesiredTarget[i] = 0;
                }
                else{
                    SetPWM(i, 0);
//                    SetBrake(i);
                }
            }
            gGateTime[i]--;
        }
        flgChannel = flgChannel << 1;
    }
}

void TrapezoidalAccel(void)
{
    unsigned char i;
    if(gTimer1ms % gTzPeriod == 0){
        for(i = 0;i < USED_CHANNEL;i++){
            if(gCurrentTarget[i] != gDesiredTarget[i]){
                gCurrentTarget[i] += gDeltaTarget[i];
                if(gCurrentTarget[i] < 0)
                    SetBackward(i);
                else
                    SetForward(i);
                SetPWM(i, abs(gCurrentTarget[i]));
            }        
        } 
    }
}

