/**
 * @file      main.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "supplement.h"
#include "eeprom.h"
#include "UART.h"
#include "initialize.h"
#include "command.h"
#include "channel.h"
#include "PID.h"
#include "MCU_config.h"

#include "MS_lib/core/microshell.h"
#include "MS_lib/core/msconf.h"
#include "MS_lib/util/mscmd.h"

volatile long gCountEnc[MAX_CHANNEL];
volatile long gLastCountEnc[MAX_CHANNEL];
volatile long gDiffCount[MAX_CHANNEL];
volatile long gTimer1ms;
volatile unsigned char gFlg100ms;
long gSpeedTick[MAX_CHANNEL];

unsigned char gFlgControllingPosition;
unsigned char gFlgControlLoop;
unsigned char gFlgDebug;
unsigned char gFlgEcho;

void main()
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    MICROSHELL ms;
    MSCMD mscmd;

    char c, i;
    
    InitIO();
    InitPWM();
    InitUART();
    InitEnc();
    InitChannel();
    InitI2C();

    if (REG_SETTING_PLUG == PLUG_ON){
        gP_gain[0] = 1.8;
        gI_gain[0] = 0.6;
        gD_gain[0] = 1.6;
        write_float_to_eeprom(ADDR_EEPROM_KP_N, gP_gain[0]);
        write_float_to_eeprom(ADDR_EEPROM_KI_N, gI_gain[0]);
        write_float_to_eeprom(ADDR_EEPROM_KD_N, gD_gain[0]);
        
        gP_gain[1] = 1.6;
        gI_gain[1] = 0.4;
        gD_gain[1] = 1.0;
        write_float_to_eeprom(ADDR_EEPROM_KP_P, gP_gain[1]);
        write_float_to_eeprom(ADDR_EEPROM_KI_P, gI_gain[1]);
        write_float_to_eeprom(ADDR_EEPROM_KD_P, gD_gain[1]);
        
        gFlgTzEnable = 0;
        gTzPeriod = 100;
        gTzStep = 10;
        write_int_to_eeprom(ADDR_EEPROM_TZ_FLG, gFlgTzEnable);
        write_int_to_eeprom(ADDR_EEPROM_TZ_PRD, gTzPeriod);
        write_int_to_eeprom(ADDR_EEPROM_TZ_STP, gTzStep);

    }
    read_float_from_eeprom(ADDR_EEPROM_KP_N, &gP_gain[0]);
    read_float_from_eeprom(ADDR_EEPROM_KI_N, &gI_gain[0]);
    read_float_from_eeprom(ADDR_EEPROM_KD_N, &gD_gain[0]);

    read_float_from_eeprom(ADDR_EEPROM_KP_P, &gP_gain[1]);
    read_float_from_eeprom(ADDR_EEPROM_KI_P, &gI_gain[1]);
    read_float_from_eeprom(ADDR_EEPROM_KD_P, &gD_gain[1]);

    read_int_from_eeprom(ADDR_EEPROM_TZ_FLG, &gFlgTzEnable);
    read_int_from_eeprom(ADDR_EEPROM_TZ_PRD, &gTzPeriod);
    read_int_from_eeprom(ADDR_EEPROM_TZ_STP, &gTzStep);

    gFlgControlLoop = kOPENLOOP;
    gFlgControllingPosition = 0;
    gFlgEcho = 1;
    gFlgDebug = 0;
    
    for (i = 0;i < USED_CHANNEL;i++){
        gCountEnc[i] =  0;
        gDesiredTarget[i] = 0;
        SetPWM(i, 0);
    }
    REG_LED = LED_ON;

    ResetScreen();

    microshell_init(&ms, utx, urx, action_hook);
    mscmd_init(&mscmd, table, sizeof(table) / sizeof(table[0]), &usrobj);
    for(i = 0;i < MSCONF_MAX_INPUT_LENGTH;i++){
        buf[i] = 0x00;
    } 
    i = 0;
    while (1) {
        if(myKbhit()){
            if(gFlgEcho)
                c = myGetche();
            else
                c = myGetch();
            if ((c != '\r') && (c != '\n')){
                if (i < MSCONF_MAX_INPUT_LENGTH){
                    buf[i] = c;
                    i++;
                }
            }
            else{
                MSCMD_USER_RESULT r;
                mscmd_execute(&mscmd, buf, &r);
                myPuts("\r\n");
                if(gFlgEcho)
                    myPuts(":");
                for(i = 0;i < MSCONF_MAX_INPUT_LENGTH;i++){
                    buf[i] = 0;
                }
                i = 0;
            }
        }
        else{
            // OpenLoop
            if (gFlgControlLoop == kOPENLOOP){
                if(gFlgTzEnable == 1)
                    TrapezoidalAccel();
                
                // Now debugging
//                if(bittst(gFlgDebug, 0b00000001))
//                    PidSpeed();                
            }
            // ClosedLoop
            else{   
                PID();
                BlinkAlive();
            }
        }
    }
}
