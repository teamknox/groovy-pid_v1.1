/**
 * @file      initialze.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "eeprom.h"
#include "channel.h"

void InitIO()
{
    OSCCON = 0b01110010;    // Internal OSC(16MHz)
     
    ANSELA = 0b00000000;    // Only Digital-I/O
    ANSELB = 0b00000000;    // Only Digital-I/O
    ANSELC = 0b00000000;    // Only Digital-I/O

    TRISA  = 0b11000010;    // 
    TRISB  = 0b11011111;    // 
    TRISC  = 0b11011000;    // 
}

void InitPWM()
{
    PSTR1CON = 0b00000001;
    PSTR2CON = 0b00000001;
    
    CCPTMRS0 = 0b00000000;   // CCPx uses Timer2.
    T2CON = 0b00000011;     // Set x16 pre-scaler of TMR2
    CCP1CON = 0b00001100;
    CCP2CON = 0b00001100;
    TMR2 = 0;
    PR2 = 249;
    TMR2ON = 1;
    
    T0CON = 0b11000100;
    TMR0IE = 1;
    TMR0IP = 0;
}

void InitEnc()
{
    INTEDG0 = 0;           // RB0 interrupt on raising-edge
    INTEDG1 = 1;           // RB1 interrupt on raising-edge
    INTEDG2 = 1;           // RB2 interrupt on raising-edge
     
    INT1IP = 1;            // Set RB1 to be high-priority-interrupt
    INT2IP = 1;            // Set RB2 to be high-priority-interrupt

//    INT0IE = 1;            // Enable interrupt on RB0
    INT1IE = 1;            // Enable interrupt on RB1
    INT2IE = 1;            // Enable interrupt on RB2
    
    IPEN = 1;
    GIE = 1;
    GIEH = 1;
    GIEL = 1;
}

void InitChannel()
{
    PWM[0] = PWM0;
    PWM[1] = PWM1;
    
    Brake[0] = Brake0;
    Brake[1] = Brake1;
    
    Forward[0] = Forward0;
    Forward[1] = Forward1;

    Backward[0] = Backward0;
    Backward[1] = Backward1;
}

void InitI2C()
{
    SSPCON1 = 0x28;
    SSPSTAT = 0b00000000;
    SSPADD = 0x27;  // 0x27@100KHz, 0x09@400KHz
}
