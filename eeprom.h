/**
 * @file      eeprom.h
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef EEPROM_H
#define EEPROM_H

#include <xc.h>

enum eeprom_addr {
    // PID Parms
    ADDR_EEPROM_KP_N = 0x00,
    ADDR_EEPROM_KI_N = 0x08,
    ADDR_EEPROM_KD_N = 0x10,
    ADDR_EEPROM_KP_P = 0x18,
    ADDR_EEPROM_KI_P = 0x20,
    ADDR_EEPROM_KD_P = 0x28,
    
    // Trapezoidal Accel Parms
    ADDR_EEPROM_TZ_FLG = 0x30,
    ADDR_EEPROM_TZ_PRD = 0x32,
    ADDR_EEPROM_TZ_STP = 0x34,
};

extern unsigned char eep_read(unsigned char aEadd);
extern void eep_write(unsigned char aEadd, unsigned char aData);
extern void write_int_to_eeprom(unsigned char aEadd, int aData);
extern void read_int_from_eeprom(unsigned char aEadd, int *aData);
extern void write_float_to_eeprom(unsigned char aEadd, float aData);
extern void read_float_from_eeprom(unsigned char aEadd, float *aData);


#endif //SUPPLEMENT_H

