/**
 * @file      channel.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef CHANNEL_H
#define	CHANNEL_H

#define kTRAIPEZOID_PERIOD (1*1000)
#define kTRAIPEZOID_STEP (10)

unsigned char gFlgGate;
long gGateTime[MAX_CHANNEL];

int gFlgTzEnable;
int gTzPeriod;
int gTzStep;
int gCurrentTarget[MAX_CHANNEL];
int gDeltaTarget[MAX_CHANNEL];

extern void InitChannel();

typedef void (*fptrPWM)(unsigned int);
fptrPWM PWM[MAX_CHANNEL] = {NULL};
extern void SetPWM(unsigned char aCh, unsigned int aDutyCycle);
extern void PWM0(unsigned int aDutyCycle);
extern void PWM1(unsigned int aDutyCycle);
extern void PWM2(unsigned int aDutyCycle);
extern void PWM3(unsigned int aDutyCycle);

typedef void (*fptrBrake)();
fptrBrake Brake[MAX_CHANNEL] = {NULL};
extern void SetBrake(unsigned char aCh);
extern void Brake0();
extern void Brake1();
extern void Brake2();
extern void Brake3();

typedef void (*fptrForward)();
fptrForward Forward[MAX_CHANNEL] = {NULL};
extern void SetForward(unsigned char aCh);
extern void Forward0();
extern void Forward1();
extern void Forward2();
extern void Forward3();

typedef void (*fptrBackward)();
fptrBackward Backward[MAX_CHANNEL] = {NULL};
extern void SetBackward(unsigned char aCh);
extern void Backward0();
extern void Backward1();
extern void Backward2();
extern void Backward3();

extern void GateTimeHandling();

#endif	/* CHANNEL_H */

