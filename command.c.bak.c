/**
 * @file      command.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io_define.h"
#include "UART.h"
#include "supplement.h"
#include "eeprom.h"
#include "channel.h"
#include "PID.h"
#include "command.h"
#include "I2C.h"

extern volatile long gCountEnc[MAX_CHANNEL];
extern unsigned char gFlgControlLoop;
extern unsigned char gFlgDebug;
extern unsigned char gFlgControllingPosition;
extern volatile long gTimer1ms;
extern unsigned char gFlgEcho;


void ResetScreen()
{
    putch(0x1b);putch('[');putch('2');putch('J');
    putch(0x1b);putch('[');putch('1');putch('m');
    putch(0x1b);putch('[');putch('3');putch('6');putch('m');    
    myPuts("\r\n");
    myPuts(ASTARISKS);
    myPuts(PRODUCT_NAME);
    myPuts(VERSION);
    myPuts(COPY_RIGHT);
    myPuts(ASTARISKS);
    myPuts("\r\n");
    putch(0x1b);putch('[');putch('0');putch('m');
    myPuts(":");    
}


void utx(char c)
{
    putch(c);
}

char urx(void)
{
    return myGetch();
}

void action_hook(MSCORE_ACTION action)
{
}

MSCMD_USER_RESULT usrcmd_LED(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;

    msopt_get_argc(msopt, &argc);    
    switch (argc){
        case 1:
            printf("\r\n%d", !REG_LED);
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case '0':
                    REG_LED = LED_OFF;
                    break;
                case '1':
                    REG_LED = LED_ON;
                    break;
                default:
                    break;
            }
        default:
            break;
    }
    return 0;
}

MSCMD_USER_RESULT usrcmd_Counter(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    
    long count[4];
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    for (i = 0;i < USED_CHANNEL;i++){
        GIEH = 0;
        count[i] = gCountEnc[i];
        GIEH = 1;
    }

    switch (argc){
        case 1:
            printf("\r\n");
            for (i = 0;i < USED_CHANNEL;i++){
                printf("%ld ", count[i]);
            }
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'J':
                case 'j':
                    printf("\r\n");
                    printf("{\r\n");
                    printf("  \"gTimer1ms\":  %ld,\r\n", gTimer1ms);
                    printf("  \"gCountEnc\":[");
                    for (i = 0;i < USED_CHANNEL - 1;i++){
                        printf("%d, ", count[i]);
                    }
                    printf("%d", count[i]);
                    printf("]\r\n");
                    printf("}\r\n");
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    return 0;
}

MSCMD_USER_RESULT usrcmd_ClearScreen(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));

    ResetScreen();
    
    return 0;
}


MSCMD_USER_RESULT usrcmd_Speed(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    int speed;
    unsigned char channel, flgChPos = 0b00000001;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
        case 1:
            // gCurrentTarget should be displayed here !!
            printf("gCurrentTarget = ");
            for (i = 0;i < USED_CHANNEL;i++){
                printf("%04d ", gCurrentTarget[i]);
            }
            printf("\r\n");
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'J':
                case 'j':
                    printf("\r\n");
                    printf("{\r\n");
                    printf("  \"gTimer1ms\":  %ld,\r\n", gTimer1ms);
                    printf("  \"gCurrentTarget\":[");
                    for (i = 0;i < USED_CHANNEL - 1;i++){
                        printf("%d, ", gCurrentTarget[i]);
                    }
                    printf("%d", gCurrentTarget[i]);
                    printf("]\r\n");
                    printf("}\r\n");
                    break;
                default:
                    break;
            }
            break;
        case 3:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            channel = atoi(buf);
            if ((channel >= LEFT01) && (channel < USED_CHANNEL)){
                msopt_get_argv(msopt, 2, buf, sizeof(buf));
                speed = atoi(buf);
                gDesiredTarget[channel] = speed;
                flgChPos = flgChPos << channel;
                if (gFlgControlLoop != kOPENLOOP){
                    bitclr(gFlgControllingPosition, flgChPos);
                }
                else{
                    if(gFlgTzEnable){
                        if (gDesiredTarget[channel] > gCurrentTarget[channel]){
                            gDeltaTarget[channel] = gTzStep;
                        }
                        else{
                            gDeltaTarget[channel] = -gTzStep;
                        }
                    }
                    else{
                        if (speed < 0)
                            SetBackward(channel);
                        else
                            SetForward(channel);
                        SetPWM(channel, abs(speed));    
                    }
                }
            }
            break;
            
        case 2 + USED_CHANNEL:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            if (atoi(buf) == USED_CHANNEL){
                for (i = LEFT01;i < USED_CHANNEL;i++){
                    msopt_get_argv(msopt, 2 + i, buf, sizeof(buf));
                    speed = atoi(buf);
                    gDesiredTarget[i] = speed;
                    if (gFlgControlLoop != kOPENLOOP){
                        bitclr(gFlgControllingPosition, flgChPos);
                    }
                    else{
                        if(gFlgTzEnable){
                            if (gDesiredTarget[i] > gCurrentTarget[i]){
                                gDeltaTarget[i] = gTzStep;
                            }
                            else{
                                gDeltaTarget[i] = -gTzStep;
                            }
                        }
                        else{
                            if (speed < 0)
                                SetBackward(i);
                            else
                                SetForward(i);
                            SetPWM(i, abs(speed));    
                        }
                    }
                    flgChPos = flgChPos << 1;
                }
            }            
        default:
            break;
    }
    return 0;
}

MSCMD_USER_RESULT usrcmd_Brake(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    int speed;
    unsigned char channel;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
//        case 1:
//            // Brake status should be displayed here !
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            channel = atoi(buf);
            if ((channel >= LEFT01) && (channel < USED_CHANNEL)){   // 0-3
                SetBrake(channel);
                SetPWM(channel, 0);
            }
            else{
                for (i = LEFT01;i < USED_CHANNEL;i++){  // more than 4
                  SetBrake(i);  
                  SetPWM(i, 0);
                }
            }
            break;
            
        default:
            break;
    }
    return 0;
}

extern MSCMD_USER_RESULT usrcmd_GateTime(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    long gateTime;
    unsigned char channel;
    unsigned char flgChannel = 0b00000001;

    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
        case 1:
            // gDesiredTarget should be displayed here !!
            break;
        case 3:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            channel = atoi(buf);
            if ((channel >= LEFT01) && (channel < USED_CHANNEL)){
                msopt_get_argv(msopt, 2, buf, sizeof(buf));
                gateTime = atol(buf);
                flgChannel = flgChannel << channel;
                bitset(gFlgGate, flgChannel);
                gGateTime[channel] = gateTime;
                
            }
            break;
            
        case 2 + USED_CHANNEL:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            if (atoi(buf) == USED_CHANNEL){
                for (i = LEFT01;i < USED_CHANNEL;i++){
                    msopt_get_argv(msopt, 2 + i, buf, sizeof(buf));
                    gateTime = atol(buf);
                    bitset(gFlgGate, flgChannel);
                    gGateTime[channel] = gateTime;
                    flgChannel = flgChannel << 1;
                }
            }            
        default:
            break;       
    }

    return 0;
}

extern MSCMD_USER_RESULT usrcmd_ConstantN(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    int pidvalue;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));

    switch (argc){
        case 1:
            myPuts("\r\n");
            read_float_from_eeprom(ADDR_EEPROM_KP_N, &gP_gain[0]);
            printf("P = %.3f\r\n", gP_gain[0]);
            read_float_from_eeprom(ADDR_EEPROM_KI_N, &gI_gain[0]);
            printf("I = %.3f\r\n", gI_gain[0]);
            read_float_from_eeprom(ADDR_EEPROM_KD_N, &gD_gain[0]);
            printf("D = %.3f\r\n", gD_gain[0]);
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'P':
                case 'p':
                    read_float_from_eeprom(ADDR_EEPROM_KP_N, &gP_gain[0]);
                    printf("\r\nP = %.3f\r\n", gP_gain[0]);
                    break;
                case 'I':
                case 'i':
                    read_float_from_eeprom(ADDR_EEPROM_KI_N, &gI_gain[0]);
                    printf("\r\nI = %.3f\r\n", gI_gain[0]);
                    break;
                case 'D':
                case 'd':
                    read_float_from_eeprom(ADDR_EEPROM_KD_N, &gD_gain[0]);
                    printf("\r\nD = %.3f\r\n", gD_gain[0]);
                    break;
                default:
                    break;
            }
            break;
        case 3:
            msopt_get_argv(msopt, 2, buf, sizeof(buf));
            pidvalue = atoi(buf);
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'P':
                case 'p':
                    gP_gain[0] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KP_N, gP_gain[0]);
                    read_float_from_eeprom(ADDR_EEPROM_KP_N, &gP_gain[0]);
                    printf("\r\nP = %.3f\r\n", gP_gain[0]);
                    break;
                case 'I':
                case 'i':
                    gI_gain[0] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KI_N, gI_gain[0]);
                    read_float_from_eeprom(ADDR_EEPROM_KI_N, &gI_gain[0]);
                    printf("\r\nI = %.3f\r\n", gI_gain[0]);
                    break;
                case 'D':
                case 'd':
                    gD_gain[0] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KD_N, gD_gain[0]);
                    read_float_from_eeprom(ADDR_EEPROM_KD_N, &gD_gain[0]);
                    printf("\r\nD = %.3f\r\n", gD_gain[0]);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return 0;
}

extern MSCMD_USER_RESULT usrcmd_ConstantP(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    int pidvalue;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));

    switch (argc){
        case 1:
            myPuts("\r\n");
            read_float_from_eeprom(ADDR_EEPROM_KP_P, &gP_gain[1]);
            printf("P = %.3f\r\n", gP_gain[1]);
            read_float_from_eeprom(ADDR_EEPROM_KI_P, &gI_gain[1]);
            printf("I = %.3f\r\n", gI_gain[1]);
            read_float_from_eeprom(ADDR_EEPROM_KD_P, &gD_gain[1]);
            printf("D = %.3f\r\n", gD_gain[1]);
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'P':
                case 'p':
                    read_float_from_eeprom(ADDR_EEPROM_KP_P, &gP_gain[1]);
                    printf("\r\nP = %.3f\r\n", gP_gain[1]);
                    break;
                case 'I':
                case 'i':
                    read_float_from_eeprom(ADDR_EEPROM_KI_P, &gI_gain[1]);
                    printf("\r\nI = %.3f\r\n", gI_gain[1]);
                    break;
                case 'D':
                case 'd':
                    read_float_from_eeprom(ADDR_EEPROM_KD_P, &gD_gain[1]);
                    printf("\r\nD = %.3f\r\n", gD_gain[1]);
                    break;
                default:
                    break;
            }
            break;
        case 3:
            msopt_get_argv(msopt, 2, buf, sizeof(buf));
            pidvalue = atoi(buf);
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case 'P':
                case 'p':
                    gP_gain[1] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KP_P, gP_gain[1]);
                    read_float_from_eeprom(ADDR_EEPROM_KP_P, &gP_gain[1]);
                    printf("\r\nP = %.3f\r\n", gP_gain[1]);
                    break;
                case 'I':
                case 'i':
                    gI_gain[1] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KI_P, gI_gain[1]);
                    read_float_from_eeprom(ADDR_EEPROM_KI_P, &gI_gain[1]);
                    printf("\r\nI = %.3f\r\n", gI_gain[1]);
                    break;
                case 'D':
                case 'd':
                    gD_gain[1] = pidvalue * 0.001;
                    write_float_to_eeprom(ADDR_EEPROM_KD_P, gD_gain[1]);
                    read_float_from_eeprom(ADDR_EEPROM_KD_P, &gD_gain[1]);
                    printf("\r\nD = %.3f\r\n", gD_gain[1]);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return 0;
}


MSCMD_USER_RESULT usrcmd_Position(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    unsigned char channel;
    long target;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
        case 1:
            printf("\r\n%ld %ld %ld %ld", gTickTarget[0], gTickTarget[1], gTickTarget[2], gTickTarget[3]);
            break;
        case 3:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            channel = atoi(buf);
            if ((channel >= LEFT01) && (channel < USED_CHANNEL)){
                msopt_get_argv(msopt, 2, buf, sizeof(buf));
                target = atol(buf);
                SetPosition(channel, target);
            }
            break;
        case 2 + USED_CHANNEL:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            if (atoi(buf) == USED_CHANNEL){
                for (i = LEFT01;i < USED_CHANNEL;i++){
                    msopt_get_argv(msopt, 2 + i, buf, sizeof(buf));
                    target = atol(buf);
                    SetPosition(i, target);
                }
            }
            break;
        default:
            break;       
    }
    return 0;
}

MSCMD_USER_RESULT usrcmd_Mode(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    unsigned char mode;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 1, buf, sizeof(buf));

    if (argc == 1)
        printf("\r\n%d", gFlgControlLoop);
    
    mode = atoi(buf);
    
    if((mode >= 0) && (mode <= 4)){
        gFlgControlLoop = mode;
        REG_LED = LED_ON;
        for (i = 0;i < USED_CHANNEL;i++){
            gDesiredTarget[i] = 0;
            gCurrentTarget[i] = 0;
            SetPWM(i, 0);;
            SetPosition(i, 0);;
        }
    }
    
    return 0;
}

MSCMD_USER_RESULT usrcmd_TA(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
        case 1:
            // Set data should be displayed here !!
            printf("\r\n");
            printf("gFlgTzEnable = %d\r\n", gFlgTzEnable);
            printf("gTzPeriod = %d\r\n", gTzPeriod);
            printf("gTzStep = %d\r\n", gTzStep);
            break;
        case 4:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            gFlgTzEnable = atoi(buf);
            write_int_to_eeprom(ADDR_EEPROM_TZ_FLG, gFlgTzEnable);
            msopt_get_argv(msopt, 2, buf, sizeof(buf));
            gTzPeriod = atoi(buf);
            write_int_to_eeprom(ADDR_EEPROM_TZ_PRD, gTzPeriod);
            msopt_get_argv(msopt, 3, buf, sizeof(buf));
            gTzStep = atoi(buf);
            write_int_to_eeprom(ADDR_EEPROM_TZ_STP, gTzStep);
            break;
    }
    
    return 0;
}

MSCMD_USER_RESULT usrcmd_Echo(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;

    msopt_get_argc(msopt, &argc);    
    switch (argc){
        case 1:
            printf("\r\n%d", gFlgEcho);
            break;
        case 2:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            switch (buf[0]){
                case '0':
                    gFlgEcho = 0;
                    break;
                case '1':
                    gFlgEcho = 1;
                    break;
                default:
                    break;
            }
        default:
            break;
    }
    return 0;
}

MSCMD_USER_RESULT usrcmd_I2C(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    unsigned char addr, data;
/*    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 1, buf, sizeof(buf));
    
    switch (argc){
        case 0:
            break;
        case 1:
            break;
    }
*/
    return 0;
}

MSCMD_USER_RESULT usrcmd_Debug(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    unsigned char debugPos, value;
    unsigned char flgDebug = 0b00000001;

    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 1, buf, sizeof(buf));

    switch (argc){
        case 1:
            printf("\r\n%0x", gFlgDebug);
            break;
        case 3:
            msopt_get_argv(msopt, 1, buf, sizeof(buf));
            debugPos = atoi(buf);
            msopt_get_argv(msopt, 2, buf, sizeof(buf));
            value = atoi(buf);
            flgDebug = flgDebug << debugPos;
            if (value)
                bitset(gFlgDebug, flgDebug);
            else
                bitclr(gFlgDebug, flgDebug);
            printf("\r\n%0x", gFlgDebug);
            break;
        default:
            break;
    }

    return 0;
}

MSCMD_USER_RESULT usrcmd_Version(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    char buf[MSCONF_MAX_INPUT_LENGTH];
    int argc;
    int i;
    
    msopt_get_argc(msopt, &argc);
    msopt_get_argv(msopt, 0, buf, sizeof(buf));
    
    switch (argc){
        case 1:
            myPuts("\r\n");
            myPuts(VERSION);
            break;
        default:
            break;
    }    
    return 0;    
}

MSCMD_USER_RESULT usrcmd_Help(MSOPT *msopt, MSCMD_USER_OBJECT usrobj)
{
    return 0;
}

