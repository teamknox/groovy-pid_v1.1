/**
 * @file      I2C.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"
#include "I2C.h"

void I2CCheckStartCondition()
{
    SSPCON2bits.SEN = 1;
	while (SSPCON2bits.SEN);
}

void I2CCheckRepeatedStartCondition()
{
    SSPIF = 0;
	SSPCON2bits.RSEN = 1;
	while (SSPCON2bits.RSEN);
}

void I2CCheckStopCondition()
{
    SSPIF = 0;
	SSPCON2bits.PEN = 1;
	while (SSPCON2bits.PEN);
	SSPIF = 0;    
}

void I2CTransmitOneByte(unsigned char aByte)
{
    SSPIF = 0;
	SSPBUF = aByte;
	while (!SSPIF);
}

unsigned char I2CReceiveOneByte()
{
    SSPIF = 0;
	SSPCON2bits.RCEN = 1;
	while (SSPCON2bits.RCEN);

	return(SSPBUF);
}

unsigned char I2CCheckAck()
{
    unsigned char i2cData;

	if (SSPCON2bits.ACKSTAT) {
        // Acknowledge was not received from slave.
		i2cData = 0xFF;
	} else {
        // Acknowledge was received from slave.
		i2cData = 0x00;
	}

	return(i2cData);    
}

void I2CTransmitAck()
{
    SSPCON2bits.ACKDT = 0;
	SSPCON2bits.ACKEN = 1;
	while (SSPCON2bits.ACKEN);    
}

void I2CTransmitNack()
{
    SSPCON2bits.ACKDT = 1;
	SSPCON2bits.ACKEN = 1;
	while (SSPCON2bits.ACKEN);
}

