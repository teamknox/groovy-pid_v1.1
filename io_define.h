/**
 * @file      io_define.h
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef IODEFINE_H
#define IODEFINE_H

#include <xc.h>

#define _XTAL_FREQ 16000000   // for __delay functions = 16MHz
#define MAX_CHANNEL 4

#define ASTARISKS "    ***    "
#define PRODUCT_NAME "Groovy-PID"
#define VERSION " Ver.1.1.0"
#define COPY_RIGHT " by (c) 2019 Omiya-Giken LLC"

#define bitset(var,bitpattern) ((var) |= (bitpattern))
#define bitclr(var,bitpattern) ((var) &= ~(bitpattern))
#define bittst(var,bitpattern) (var & (bitpattern))
#define bittgl(var,bitpattern) ((var) ^= (bitpattern))

enum channel {
    LEFT01 = 0,
    LEFT11,
    
    USED_CHANNEL,
};

#define REG_LED LATA0
enum led_onoff {
    LED_ON = 0,
    LED_OFF
};

#define REG_SETTING_PLUG PORTAbits.RA1
enum plug_status {
    PLUG_ON = 0,
    PLUG_OFF
};

#define QEI_PORT    PORTB

#define TEST_PIN LATC0

#define INP1_L  LATA2
#define INP2_L  LATA3
#define INP3_L  LATA4
#define INP4_L  LATA5

#define FLG_QEIL01_BIT 0x10
#define FLG_QEIL11_BIT 0x08

// Left-side (@Normal mounting)
#define REG_0_PWM_H CCPR2L
#define REG_0_PWM_M CCP2CONbits.DC2B1
#define REG_0_PWM_L CCP2CONbits.DC2B0
// Right-side (@Normal mounting)
#define REG_1_PWM_H CCPR1L
#define REG_1_PWM_M CCP1CONbits.DC1B1
#define REG_1_PWM_L CCP1CONbits.DC1B0

// UART HAL
#define BAUDCONx    BAUDCON1
#define SPBRGx      SPBRG1
#define TXSTAx      TXSTA1
#define TXxIF       TX1IF
#define TXxIP       TX1IP
#define TXxIE       TX1IE
#define TXREGx      TXREG1
#define RCSTAx      RCSTA1
#define RCxIF       RC1IF
#define RCREGx      RCREG1


#endif //IODEFINE_H

