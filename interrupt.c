/**
 * @file      interrupt.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-Quatro (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include "io_define.h"

// UART.c
extern void UartTxISR(void);

// channel.c
extern void GateTimeHandling(void);

extern volatile long gCountEnc[MAX_CHANNEL];
extern volatile long gTimer1ms;
extern volatile unsigned char gFlg100ms;

unsigned char gPort_copy;

void __interrupt() isrHigh(void)
{
    gPort_copy = QEI_PORT;
    
    if(INT1IF == 1){
        INT1IF = 0;
        if(gPort_copy & FLG_QEIL01_BIT){
            gCountEnc[LEFT01]--;
//            REG_LED = LED_ON;
        }
        else{
            gCountEnc[LEFT01]++;
//            REG_LED = LED_OFF;
        }
    }

    if(INT2IF == 1){
        INT2IF = 0;
        if(gPort_copy & FLG_QEIL11_BIT){
            gCountEnc[LEFT11]--;
//            REG_LED = LED_ON;
        }
        else{
            gCountEnc[LEFT11]++;
//            REG_LED = LED_OFF;
        }
    }

}


void __interrupt(low_priority) isrLow(void)
{
    if(TXxIF == 1 && TXxIE == 1){
        UartTxISR();
        TXxIF = 0;
    }
    
    if(TMR0IF == 1){
        TMR0IF = 0;
        
        // Setting for 1 msec tick.
        TMR0H = 0x00;
        TMR0L = 0x83;
        
        // bittgl(TEST_PIN, 0x01);
        gTimer1ms++;
        GateTimeHandling();
    }
}
