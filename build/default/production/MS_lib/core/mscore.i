# 1 "MS_lib/core/mscore.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/xc8/v2.10/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "MS_lib/core/mscore.c" 2
# 34 "MS_lib/core/mscore.c"
# 1 "MS_lib/core/mscore.h" 1
# 40 "MS_lib/core/mscore.h"
typedef enum {
    MSCORE_ACTION_IGNORE,
    MSCORE_ACTION_DISPLAYABLE,
    MSCORE_ACTION_BS,
    MSCORE_ACTION_DEL,
    MSCORE_ACTION_TAB,
    MSCORE_ACTION_INSERT,
    MSCORE_ACTION_ENTER,
    MSCORE_ACTION_CTRL_A,
    MSCORE_ACTION_CTRL_B,
    MSCORE_ACTION_CTRL_C,
    MSCORE_ACTION_CTRL_D,
    MSCORE_ACTION_CTRL_E,
    MSCORE_ACTION_CTRL_F,
    MSCORE_ACTION_CTRL_G,
    MSCORE_ACTION_CTRL_H,
    MSCORE_ACTION_CTRL_I,
    MSCORE_ACTION_CTRL_J,
    MSCORE_ACTION_CTRL_K,
    MSCORE_ACTION_CTRL_L,
    MSCORE_ACTION_CTRL_M,
    MSCORE_ACTION_CTRL_N,
    MSCORE_ACTION_CTRL_O,
    MSCORE_ACTION_CTRL_P,
    MSCORE_ACTION_CTRL_Q,
    MSCORE_ACTION_CTRL_R,
    MSCORE_ACTION_CTRL_S,
    MSCORE_ACTION_CTRL_T,
    MSCORE_ACTION_CTRL_U,
    MSCORE_ACTION_CTRL_V,
    MSCORE_ACTION_CTRL_W,
    MSCORE_ACTION_CTRL_X,
    MSCORE_ACTION_CTRL_Y,
    MSCORE_ACTION_CTRL_Z,
    MSCORE_ACTION_F1,
    MSCORE_ACTION_F2,
    MSCORE_ACTION_F3,
    MSCORE_ACTION_F4,
    MSCORE_ACTION_F5,
    MSCORE_ACTION_F6,
    MSCORE_ACTION_F7,
    MSCORE_ACTION_F8,
    MSCORE_ACTION_F9,
    MSCORE_ACTION_F10,
    MSCORE_ACTION_F11,
    MSCORE_ACTION_F12,
    MSCORE_ACTION_ARROW_UP,
    MSCORE_ACTION_ARROW_DOWN,
    MSCORE_ACTION_ARROW_LEFT,
    MSCORE_ACTION_ARROW_RIGHT,
    MSCORE_ACTION_HOME,
    MSCORE_ACTION_END,
    MSCORE_ACTION_PAGE_UP,
    MSCORE_ACTION_PAGE_DOWN,
} MSCORE_ACTION;

typedef struct {
    int keycnt;
    char keybuf[(8)];
} MSCORE;





void mscore_init(MSCORE *handle);
MSCORE_ACTION mscore_push(MSCORE *handle, char c);
# 35 "MS_lib/core/mscore.c" 2
# 1 "MS_lib/core/msconf.h" 1
# 36 "MS_lib/core/mscore.c" 2

typedef char * MSCORE_STREAM;

typedef struct {
    MSCORE_STREAM stream;
    MSCORE_ACTION action;
} MSCORE_KEYMAP;

static const MSCORE_KEYMAP kmlist[] = {




    { "\x08", MSCORE_ACTION_BS },
    { "\x7f", MSCORE_ACTION_DEL },

    { "\x09", MSCORE_ACTION_TAB },
    { "\x1b\x5b\x32\x7e", MSCORE_ACTION_INSERT },
    { "\x0d", MSCORE_ACTION_ENTER },

    { "\x01", MSCORE_ACTION_CTRL_A },
    { "\x02", MSCORE_ACTION_CTRL_B },
    { "\x03", MSCORE_ACTION_CTRL_C },
    { "\x04", MSCORE_ACTION_CTRL_D },
    { "\x05", MSCORE_ACTION_CTRL_E },
    { "\x06", MSCORE_ACTION_CTRL_F },
    { "\x07", MSCORE_ACTION_CTRL_G },
    { "\x08", MSCORE_ACTION_CTRL_H },
    { "\x09", MSCORE_ACTION_CTRL_I },
    { "\x0a", MSCORE_ACTION_CTRL_J },
    { "\x0b", MSCORE_ACTION_CTRL_K },
    { "\x0c", MSCORE_ACTION_CTRL_L },
    { "\x0d", MSCORE_ACTION_CTRL_M },
    { "\x0e", MSCORE_ACTION_CTRL_N },
    { "\x0f", MSCORE_ACTION_CTRL_O },
    { "\x10", MSCORE_ACTION_CTRL_P },
    { "\x11", MSCORE_ACTION_CTRL_Q },
    { "\x12", MSCORE_ACTION_CTRL_R },
    { "\x13", MSCORE_ACTION_CTRL_S },
    { "\x14", MSCORE_ACTION_CTRL_T },
    { "\x15", MSCORE_ACTION_CTRL_U },
    { "\x16", MSCORE_ACTION_CTRL_V },
    { "\x17", MSCORE_ACTION_CTRL_W },
    { "\x18", MSCORE_ACTION_CTRL_X },
    { "\x19", MSCORE_ACTION_CTRL_Y },
    { "\x1a", MSCORE_ACTION_CTRL_Z },


    { "\x1b\x5b\x31\x31\x7e", MSCORE_ACTION_F1 },
    { "\x1b\x5b\x31\x32\x7e", MSCORE_ACTION_F2 },
    { "\x1b\x5b\x31\x33\x7e", MSCORE_ACTION_F3 },
    { "\x1b\x5b\x31\x34\x7e", MSCORE_ACTION_F4 },
    { "\x1b\x5b\x31\x35\x7e", MSCORE_ACTION_F5 },
    { "\x1b\x5b\x31\x37\x7e", MSCORE_ACTION_F6 },
    { "\x1b\x5b\x31\x38\x7e", MSCORE_ACTION_F7 },
    { "\x1b\x5b\x31\x39\x7e", MSCORE_ACTION_F8 },
    { "\x1b\x5b\x32\x30\x7e", MSCORE_ACTION_F9 },
    { "\x1b\x5b\x32\x31\x7e", MSCORE_ACTION_F10 },
    { "\x1b\x5b\x32\x32\x7e", MSCORE_ACTION_F11 },
    { "\x1b\x5b\x32\x33\x7e", MSCORE_ACTION_F12 },
    { "\x1b\x4f\x50", MSCORE_ACTION_F1 },
    { "\x1b\x4f\x51", MSCORE_ACTION_F2 },
    { "\x1b\x4f\x52", MSCORE_ACTION_F3 },
    { "\x1b\x4f\x53", MSCORE_ACTION_F4 },
    { "\x1b\x5b\x31\x35\x7e", MSCORE_ACTION_F5 },
    { "\x1b\x5b\x31\x37\x7e", MSCORE_ACTION_F6 },
    { "\x1b\x5b\x31\x38\x7e", MSCORE_ACTION_F7 },
    { "\x1b\x5b\x31\x39\x7e", MSCORE_ACTION_F8 },
    { "\x1b\x5b\x32\x31\x7e", MSCORE_ACTION_F9 },
    { "\x1b\x5b\x32\x32\x7e", MSCORE_ACTION_F10 },
    { "\x1b\x5b\x32\x33\x7e", MSCORE_ACTION_F11 },
    { "\x1b\x5b\x32\x34\x7e", MSCORE_ACTION_F12 },


    { "\x1b\x5b\x41", MSCORE_ACTION_ARROW_UP },
    { "\x1b\x5b\x42", MSCORE_ACTION_ARROW_DOWN },
    { "\x1b\x5b\x43", MSCORE_ACTION_ARROW_RIGHT },
    { "\x1b\x5b\x44", MSCORE_ACTION_ARROW_LEFT },


    { "\x1b\x4f\x48", MSCORE_ACTION_HOME },
    { "\x1b\x4f\x46", MSCORE_ACTION_END },
    { "\x1b[1~", MSCORE_ACTION_HOME },
    { "\x1b[4~", MSCORE_ACTION_END },


    { "\x1b\x5b\x35\x7e", MSCORE_ACTION_PAGE_UP },
    { "\x1b\x5b\x36\x7e", MSCORE_ACTION_PAGE_DOWN },

};

static int buf_length(char *buf)
{
    int n = 0;
    while (*buf++) {
        n++;
    }
    return n;
}

static int buf_match(char *a, char *b, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        if (*a++ != *b++) {
            return 0;
        }
    }
    return 1;
}

static int buf_clear(char *buf, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        buf[i] = 0;
    }
    return 1;
}

void mscore_init(MSCORE *handle)
{
    buf_clear(handle->keybuf, (8));
    handle->keycnt = 0;
}

MSCORE_ACTION mscore_push(MSCORE *handle, char c)
{
    int match = 0;
    handle->keybuf[handle->keycnt++] = c;
    const MSCORE_KEYMAP *p = &kmlist[0];
    const int N = sizeof(kmlist) / sizeof(kmlist[0]);
    int i;
    for (i = 0; i < N; i++) {
        if (handle->keycnt == buf_length(p->stream)) {
            if (buf_match(p->stream, handle->keybuf, handle->keycnt)) {
                handle->keycnt = 0;
                buf_clear(handle->keybuf, (8));
                return p->action;
            }
        } else {
            if (buf_match(p->stream, handle->keybuf, handle->keycnt)) {
                match = 1;
                break;
            }
        }
        p++;
    }
    if (!match) {
        handle->keycnt = 0;
        buf_clear(handle->keybuf, (8));
        if ((' ' <= c) && (c <= '~')) {
            return MSCORE_ACTION_DISPLAYABLE;
        }
    }
    return MSCORE_ACTION_IGNORE;
}
