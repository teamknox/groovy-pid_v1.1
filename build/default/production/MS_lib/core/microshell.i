# 1 "MS_lib/core/microshell.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/xc8/v2.10/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "MS_lib/core/microshell.c" 2
# 34 "MS_lib/core/microshell.c"
# 1 "MS_lib/core/microshell.h" 1
# 37 "MS_lib/core/microshell.h"
# 1 "MS_lib/core/mscore.h" 1
# 40 "MS_lib/core/mscore.h"
typedef enum {
    MSCORE_ACTION_IGNORE,
    MSCORE_ACTION_DISPLAYABLE,
    MSCORE_ACTION_BS,
    MSCORE_ACTION_DEL,
    MSCORE_ACTION_TAB,
    MSCORE_ACTION_INSERT,
    MSCORE_ACTION_ENTER,
    MSCORE_ACTION_CTRL_A,
    MSCORE_ACTION_CTRL_B,
    MSCORE_ACTION_CTRL_C,
    MSCORE_ACTION_CTRL_D,
    MSCORE_ACTION_CTRL_E,
    MSCORE_ACTION_CTRL_F,
    MSCORE_ACTION_CTRL_G,
    MSCORE_ACTION_CTRL_H,
    MSCORE_ACTION_CTRL_I,
    MSCORE_ACTION_CTRL_J,
    MSCORE_ACTION_CTRL_K,
    MSCORE_ACTION_CTRL_L,
    MSCORE_ACTION_CTRL_M,
    MSCORE_ACTION_CTRL_N,
    MSCORE_ACTION_CTRL_O,
    MSCORE_ACTION_CTRL_P,
    MSCORE_ACTION_CTRL_Q,
    MSCORE_ACTION_CTRL_R,
    MSCORE_ACTION_CTRL_S,
    MSCORE_ACTION_CTRL_T,
    MSCORE_ACTION_CTRL_U,
    MSCORE_ACTION_CTRL_V,
    MSCORE_ACTION_CTRL_W,
    MSCORE_ACTION_CTRL_X,
    MSCORE_ACTION_CTRL_Y,
    MSCORE_ACTION_CTRL_Z,
    MSCORE_ACTION_F1,
    MSCORE_ACTION_F2,
    MSCORE_ACTION_F3,
    MSCORE_ACTION_F4,
    MSCORE_ACTION_F5,
    MSCORE_ACTION_F6,
    MSCORE_ACTION_F7,
    MSCORE_ACTION_F8,
    MSCORE_ACTION_F9,
    MSCORE_ACTION_F10,
    MSCORE_ACTION_F11,
    MSCORE_ACTION_F12,
    MSCORE_ACTION_ARROW_UP,
    MSCORE_ACTION_ARROW_DOWN,
    MSCORE_ACTION_ARROW_LEFT,
    MSCORE_ACTION_ARROW_RIGHT,
    MSCORE_ACTION_HOME,
    MSCORE_ACTION_END,
    MSCORE_ACTION_PAGE_UP,
    MSCORE_ACTION_PAGE_DOWN,
} MSCORE_ACTION;

typedef struct {
    int keycnt;
    char keybuf[(8)];
} MSCORE;





void mscore_init(MSCORE *handle);
MSCORE_ACTION mscore_push(MSCORE *handle, char c);
# 38 "MS_lib/core/microshell.h" 2

typedef void (*MICROSHELL_UART_PUTC)(char c);
typedef char (*MICROSHELL_UART_GETC)(void);
typedef void (*MICROSHELL_ACTION_HOOK)(MSCORE_ACTION action);

typedef struct {
    MSCORE mscore;
    MICROSHELL_UART_PUTC uart_putc;
    MICROSHELL_UART_GETC uart_getc;
    MICROSHELL_ACTION_HOOK action_hook;
} MICROSHELL;





void microshell_init(MICROSHELL *handle, MICROSHELL_UART_PUTC uart_putc, MICROSHELL_UART_GETC uart_getc, MICROSHELL_ACTION_HOOK action_hook);
char *microshell_getline(MICROSHELL *handle, char *buf, int siz);
# 35 "MS_lib/core/microshell.c" 2
# 124 "MS_lib/core/microshell.c"
static char *copy_forward(char *buf, int ofs_src, int ofs_des, int siz)
{
    int i;
    char *p_src = buf + ofs_src;
    char *p_des = buf + ofs_des;
    for (i = 0; i < siz; i++) {
        *p_des++ = *p_src++;
    }
    return buf;
}

static char *copy_backward(char *buf, int ofs_src, int ofs_des, int siz)
{
    int i;
    char *p_src = buf + ofs_src + siz;
    char *p_des = buf + ofs_des + siz;
    for (i = 0; i < siz; i++) {
        *p_des-- = *p_src--;
    }
    return buf;
}

static char *clean_buffer(char *buf, int siz)
{
    int i;
    char *p = buf;
    for (i = 0; i < siz; i++) {
        *p++ = 0;
    }
    return buf;
}

static int print_buffer(MICROSHELL *handle, char *buf, int ofs)
{
    int cnt = 0;
    char *p = buf + ofs;
    while (*p) {
        handle->uart_putc(*p++);
        cnt++;
    }
    return cnt;
}

static int print_char(MICROSHELL *handle, char c)
{
    handle->uart_putc(c);
    return 1;
}

static void print_return(MICROSHELL *handle)
{
    handle->uart_putc('\r');
    handle->uart_putc('\n');
}

static void cursor_left(MICROSHELL *handle, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        handle->uart_putc('\x1B');
        handle->uart_putc('[');
        handle->uart_putc('D');
    }
}

static void cursor_right(MICROSHELL *handle, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        handle->uart_putc('\x1B');
        handle->uart_putc('[');
        handle->uart_putc('C');
    }
}

void microshell_init(MICROSHELL *handle, MICROSHELL_UART_PUTC uart_putc, MICROSHELL_UART_GETC uart_getc, MICROSHELL_ACTION_HOOK action_hook)
{
    mscore_init(&(handle->mscore));
    handle->uart_putc = uart_putc;
    handle->uart_getc = uart_getc;
    handle->action_hook = action_hook;
}

char *microshell_getline(MICROSHELL *handle, char *buf, int siz)
{
    char *ptr = buf;
    int pos = 0;
    int len = 0;
    clean_buffer(buf, siz);
    while (1) {
        char c = handle->uart_getc();
        MSCORE_ACTION action = mscore_push(&(handle->mscore), c);
        if (handle->action_hook) {
            handle->action_hook(action);
        }






        switch (action) {
            case MSCORE_ACTION_DISPLAYABLE:
                {
                    int n = 0;
                    copy_backward(buf, pos - 1, pos - 0, len - pos + 1);
                    handle->uart_putc(c);
                    *ptr++ = c;
                    pos++;
                    len++;
                    n += print_buffer(handle, buf, pos);
                    cursor_left(handle, n);
                    if (len >= siz - 1) {
                        print_return(handle);
                        goto end;
                    }
                }
                break;
            case MSCORE_ACTION_ENTER:
            case MSCORE_ACTION_CTRL_J:
                print_return(handle);
                goto end;
                break;
            case MSCORE_ACTION_CTRL_C:
                clean_buffer(buf, siz);
                print_char(handle, '^');
                print_char(handle, 'C');
                print_return(handle);
                goto end;
                break;
            case MSCORE_ACTION_BS:
            case MSCORE_ACTION_CTRL_H:
                if (pos > 0) {
                    int n = 0;
                    copy_forward(buf, pos, pos - 1, len - pos + 1);
                    ptr--;
                    pos--;
                    len--;
                    cursor_left(handle, 1);
                    n += print_buffer(handle, buf, pos);
                    n += print_char(handle, ' ');
                    cursor_left(handle, n);
                }
                break;
            case MSCORE_ACTION_DEL:
            case MSCORE_ACTION_CTRL_D:
                if (len > 0) {
                    int n = 0;
                    copy_forward(buf, pos + 1, pos + 0, len - pos + 1);
                    len--;
                    n += print_buffer(handle, buf, pos);
                    n += print_char(handle, ' ');
                    cursor_left(handle, n);
                }
                break;
            case MSCORE_ACTION_ARROW_LEFT:
            case MSCORE_ACTION_CTRL_B:
                if (pos > 0) {
                    ptr--;
                    pos--;
                    cursor_left(handle, 1);
                }
                break;
            case MSCORE_ACTION_ARROW_RIGHT:
            case MSCORE_ACTION_CTRL_F:
                if (pos < len) {
                    ptr++;
                    pos++;
                    cursor_right(handle, 1);
                }
                break;
            case MSCORE_ACTION_HOME:
            case MSCORE_ACTION_CTRL_A:
                if (pos > 0) {
                    int n = pos;
                    ptr -= n;
                    pos = 0;
                    cursor_left(handle, n);
                }
                break;
            case MSCORE_ACTION_END:
            case MSCORE_ACTION_CTRL_E:
                if (pos < len) {
                    int n = len - pos;
                    ptr += n;
                    pos = len;
                    cursor_right(handle, n);
                }
                break;
            default:
                break;
        }
    }

end:
    return buf;
}
