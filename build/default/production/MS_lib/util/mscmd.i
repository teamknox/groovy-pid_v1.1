# 1 "MS_lib/util/mscmd.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/xc8/v2.10/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "MS_lib/util/mscmd.c" 2
# 34 "MS_lib/util/mscmd.c"
# 1 "MS_lib/util/msopt.h" 1
# 37 "MS_lib/util/msopt.h"
# 1 "MS_lib/util/../../MS_lib/core/msconf.h" 1
# 38 "MS_lib/util/msopt.h" 2

typedef struct {
    char *head;
    int length;
} MSOPT_ARGINFO;

typedef struct {
    unsigned int initcode;
    int argc;
    char argv[(32)];
    MSOPT_ARGINFO info[(((32) / 2) + 1)];
} MSOPT;

typedef enum {
    MSOPT_RESULT_OK = 0,
    MSOPT_RESULT_ERROR_ILLEGAL_OBJECT,
    MSOPT_RESULT_ERROR_ILLEGAL_INDEX_NUMBER,
    MSOPT_RESULT_ERROR_TOO_MUCH_ARGUMENTS,
    MSOPT_RESULT_ERROR_BUFFER_SIZE,
} MSOPT_RESULT;





MSOPT_RESULT msopt_init(MSOPT *handle, char *text);
MSOPT_RESULT msopt_get_argc(MSOPT *handle, int *argc);
MSOPT_RESULT msopt_get_argv(MSOPT *handle, int index, char *bufptr, int bufsiz);
# 35 "MS_lib/util/mscmd.c" 2
# 1 "MS_lib/util/mscmd.h" 1
# 39 "MS_lib/util/mscmd.h"
typedef int MSCMD_USER_RESULT;
typedef void * MSCMD_USER_OBJECT;

typedef MSCMD_USER_RESULT (*MSCMD_FUNC)(MSOPT *msopt, MSCMD_USER_OBJECT usrobj);

typedef struct {
    char *argv0;
    MSCMD_FUNC func;
} MSCMD_COMMAND_TABLE;

typedef struct {
    MSCMD_COMMAND_TABLE *command_table;
    int command_count;
    MSCMD_USER_OBJECT usrobj;
} MSCMD;





int mscmd_init(MSCMD *handle, MSCMD_COMMAND_TABLE *command_table, int command_count, MSCMD_USER_OBJECT usrobj);
int mscmd_execute(MSCMD *handle, char *text, MSCMD_USER_RESULT *result);
# 36 "MS_lib/util/mscmd.c" 2
# 1 "MS_lib/util/ntlibc.h" 1
# 41 "MS_lib/util/ntlibc.h"
int ntlibc_strlen(const char *s);
char *ntlibc_strcpy(char *des, const char *src);
char *ntlibc_strcat(char *des, const char *src);
int ntlibc_strcmp(const char *s1, const char *s2);
int ntlibc_stricmp(const char *s1, const char *s2);
int ntlibc_strncmp(const char *s1, const char *s2, int n);
int ntlibc_isdigit(int c);
int ntlibc_isalpha(int c);
int ntlibc_iscntrl(int c);
int ntlibc_toupper(int c);
int ntlibc_tolower(int c);
int ntlibc_atoi(const char *nptr);
char *ntlibc_strchr(const char *s, int c);
char *ntlibc_utoa(unsigned int value, char *s, int radix);
char *ntlibc_itoa(int value, char *s, int radix);
# 37 "MS_lib/util/mscmd.c" 2

int mscmd_init(MSCMD *handle, MSCMD_COMMAND_TABLE *command_table, int command_count, MSCMD_USER_OBJECT usrobj)
{
    handle->command_table = command_table;
    handle->command_count = command_count;
    handle->usrobj = usrobj;
    return 0;
}

int mscmd_execute(MSCMD *handle, char *text, MSCMD_USER_RESULT *result)
{
    MSOPT msopt;
    int argc = 0;
    char argv0[(32)];

    msopt_init(&msopt, text);
    msopt_get_argc(&msopt, &argc);
    if (argc == 0) {
        return -1;
    }
    MSCMD_COMMAND_TABLE *p = handle->command_table;
    int i;
    for (i = 0; i < handle->command_count; i++) {
        msopt_get_argv(&msopt, 0, argv0, sizeof(argv0));
        if (ntlibc_strcmp(argv0, p->argv0) == 0) {
            *result = p->func(&msopt, handle->usrobj);
            return 0;
        }
        p++;
    }
    return -2;
}
