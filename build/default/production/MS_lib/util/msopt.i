# 1 "MS_lib/util/msopt.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/Applications/microchip/xc8/v2.10/pic/include/language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "MS_lib/util/msopt.c" 2
# 34 "MS_lib/util/msopt.c"
# 1 "MS_lib/util/msopt.h" 1
# 37 "MS_lib/util/msopt.h"
# 1 "MS_lib/util/../../MS_lib/core/msconf.h" 1
# 38 "MS_lib/util/msopt.h" 2

typedef struct {
    char *head;
    int length;
} MSOPT_ARGINFO;

typedef struct {
    unsigned int initcode;
    int argc;
    char argv[(32)];
    MSOPT_ARGINFO info[(((32) / 2) + 1)];
} MSOPT;

typedef enum {
    MSOPT_RESULT_OK = 0,
    MSOPT_RESULT_ERROR_ILLEGAL_OBJECT,
    MSOPT_RESULT_ERROR_ILLEGAL_INDEX_NUMBER,
    MSOPT_RESULT_ERROR_TOO_MUCH_ARGUMENTS,
    MSOPT_RESULT_ERROR_BUFFER_SIZE,
} MSOPT_RESULT;





MSOPT_RESULT msopt_init(MSOPT *handle, char *text);
MSOPT_RESULT msopt_get_argc(MSOPT *handle, int *argc);
MSOPT_RESULT msopt_get_argv(MSOPT *handle, int index, char *bufptr, int bufsiz);
# 35 "MS_lib/util/msopt.c" 2




static char *skip_white_space(char *text)
{
    char *p = text;
    while ((((*p) == ' ') || ((*p) == '\t') || ((*p) == '\r') || ((*p) == '\n'))) {
        if (*p == 0) {
            break;
        }
        p++;
    }
    return p;
}

static char *skip_text_string(char *text, int *length)
{
    char *p = text;
    int count = 0;
    while (!(((*p) == ' ') || ((*p) == '\t') || ((*p) == '\r') || ((*p) == '\n'))) {
        if (*p == 0) {
            break;
        }
        p++;
        count++;
    }
    *length = count;
    return p;
}

MSOPT_RESULT msopt_init(MSOPT *handle, char *text)
{
    char *src = text;
    char *des = handle->argv;
    while (*src) {
        *des++ = *src++;
    }
    *des = 0;
    handle->argc = 0;
    char *p = handle->argv;
    while (1) {
        if (handle->argc >= (((32) / 2) + 1)) {
            return MSOPT_RESULT_ERROR_TOO_MUCH_ARGUMENTS;
        }
        p = skip_white_space(p);
        if (*p == 0) {
            break;
        }
        handle->info[handle->argc].head = p;
        p = skip_text_string(p, &(handle->info[handle->argc].length));
        handle->argc++;
    }
    handle->initcode = (0x97c0);
    return MSOPT_RESULT_OK;
}

MSOPT_RESULT msopt_get_argc(MSOPT *handle, int *argc)
{
    if (handle->initcode != (0x97c0)) {
        *argc = 0;
        return MSOPT_RESULT_ERROR_ILLEGAL_OBJECT;
    }
    *argc = handle->argc;
    return MSOPT_RESULT_OK;
}

MSOPT_RESULT msopt_get_argv(MSOPT *handle, int index, char *bufptr, int bufsiz)
{
    *bufptr = 0;
    if (handle->argc <= index) {
        return MSOPT_RESULT_ERROR_ILLEGAL_INDEX_NUMBER;
    }
    char *src = handle->info[index].head;
    char *des = bufptr;
    int i;
    for (i = 0; i < handle->info[index].length; i++) {
        if (i >= bufsiz - 1) {
            *des = 0;
            return MSOPT_RESULT_ERROR_BUFFER_SIZE;
        }
        *des++ = *src++;
    }
    *des = 0;
    return MSOPT_RESULT_OK;
}
