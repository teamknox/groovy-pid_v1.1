/**
 * @file      PID.h
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-PID (Version 1.0.0)
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2019 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef PID_H
#define PID_H

#define kPROPOTIONAL_N    (gP_gain[0])
#define kINTEGRAL_N       (gI_gain[0])
#define kDIFFERENTIAL_N   (gD_gain[0])
#define kPROPOTIONAL_P  (gP_gain[1])
#define kINTEGRAL_P     (gI_gain[1])
#define kDIFFERENTIAL_P (gD_gain[1])

#define STOP_DEADBAND       1
#define PCONTROL_DEADBAND   5

#define MIN_SPEED   1
#define MAX_STEP    1

#define MAX_PWM  1023
#define MIN_PWM -1023

enum running_mode {
    kOPENLOOP,
    kP_Control,
    kPI_Control,
    kPD_Control,
    kPID_Control,
    kCLOSEDLOOP
};

#define FLG_CONTROLLING_LEFT_POSITION   0b00000001
#define FLG_CONTROLLING_RIGHT_POSITION  0b00000010

#define DEBUG_ALL 0
#define DEBUG 0

#define kTRAIPEZOIDE_STEP (1*10)
#define kDELTA_STEP (1)

long gLastCount[MAX_CHANNEL];
long gErrorIntegral[MAX_CHANNEL];
long gLastError[MAX_CHANNEL];

long gTickTarget[MAX_CHANNEL];
int gDesiredTarget[MAX_CHANNEL];
float gP_gain[2], gI_gain[2], gD_gain[2];
long gError[MAX_CHANNEL];

extern void PID(void);
extern void PidSpeed(void);
extern void PidPosition(void);
extern void TrapezoidalAccel(void);

extern void SetPosition(unsigned char aCh, long aNewTarget);
extern unsigned char MinIndex(unsigned char aLen, long *aNums);


#endif //PID_H
